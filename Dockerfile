FROM python:3.7.3-alpine3.10

RUN apk add --no-cache openjdk8

ENV JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk
ENV PATH="$JAVA_HOME/bin:${PATH}"

RUN mkdir -p /usr/src/app
WORKDIR /usr/src
COPY java_executor.py java_executor.py
WORKDIR /usr/src/app

CMD ["python3", "../java_executor.py"]
