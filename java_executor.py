import os
import sys
import subprocess
from subprocess import PIPE

COMMAND = 'COMMAND'
COMPILE = 'COMPILE'
RUN_TESTS = 'RUN_TESTS'

TASK_NAME = os.environ.get('TASK_NAME')
TEST_DIR = f'test_data/{TASK_NAME}'

SOURCE_DIR = 'src'
OUTPUT_DIR = 'out'
BUILD_DIR = 'build'

JAR_NAME = 'Main.jar'
MAIN_FILE = 'Main'

if __name__ == '__main__':
    if os.environ.get('COMMAND') == COMPILE:
        os.makedirs(f'{TASK_NAME}/out', exist_ok=True)
        os.makedirs(BUILD_DIR, exist_ok=True)
        compile_process = subprocess.run(f'find {TASK_NAME}/{SOURCE_DIR} -name "*.java" | '
                                         f'xargs -I{{}} javac -sourcepath {TASK_NAME}/{SOURCE_DIR} '
                                         f'-d {TASK_NAME}/{OUTPUT_DIR} {{}}',
                                         shell=True)
        if compile_process.returncode != 0:
            print('Error during compiling program.')
            sys.exit(1)
        jar_process = subprocess.run(f'jar cvf {BUILD_DIR}/{JAR_NAME} -C {TASK_NAME}/{OUTPUT_DIR} .',
                                     shell=True)
        if jar_process.returncode != 0:
            print('Error during JARing program.')
            sys.exit(1)
    elif os.environ.get('COMMAND') == RUN_TESTS:
        tests_dirs_names = os.listdir(TEST_DIR)
        print('Start running tests.')
        for test in tests_dirs_names:
            print(test, end=' ')
            in_file = os.path.join(TEST_DIR, test, test + '.in')
            process = subprocess.run(f'cat {in_file} | xargs -I{{}} java -cp {BUILD_DIR}/{JAR_NAME} {MAIN_FILE} {{}}',
                                     shell=True, stdout=PIPE, stderr=PIPE)
            if process.returncode != 0:
                print('Error during execution program.')
                exit(1)
            with open(os.path.join(TEST_DIR, test, test + '.out'), 'r') as out_file:
                if process.stdout.decode('utf-8') == out_file.read():
                    print('OK')
                else:
                    print('Output mismatch')
                    exit(1)
        print('End running tests.')
    else:
        print(os.environ.get('COMMAND'))
        raise Exception('Command not supported.')
